﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
namespace Uber.Website.App_Start
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
          
            bundles.Add(new StyleBundle("~/Content/bootstrapBundle").Include(
                            "~/Content/bootstrap.css",
                            "~/Content/bootstrap-theme.css"));
        

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/HistoryController").Include(
                   "~/Scripts/App/HistoryController.js"));

            bundles.Add(new ScriptBundle("~/bundles/DataTable").Include(
              "~/Scripts/App/DataTables/datatables.js"));

            bundles.Add(new StyleBundle("~/bundles/DataTable/css").Include(
                       "~/Scripts/App/DataTables/datatables.css"));


        }
    }
}