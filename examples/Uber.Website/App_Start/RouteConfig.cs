﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Uber.Website
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            //            routes.MapRoute(
            //name: "HistoryGetHistory",
            //url: "{controller}/{action}/{offset}/{limit}",
            //defaults: new { controller = "History", action = "GetHistory", offset = UrlParameter.Optional, limit = UrlParameter.Optional }
            //);
            routes.MapRoute(
name: "HistoryGetHistory",
url: "{controller}/{action}",
defaults: new { controller = "History", action = "GetHistory" }
);
            routes.MapRoute(
             name: "PriceEstimate",
             url: "{controller}/{action}/{startLatitude}/{startLongitude}/{endLatitude}/{endLongitude}",
             defaults: new { controller = "Home", action = "Estimate", startLatitude = UrlParameter.Optional, startLongitude = UrlParameter.Optional, endLatitude = UrlParameter.Optional, endLongitude = UrlParameter.Optional }
         );
            routes.MapRoute(
            name: "AuthController",
            url: "{controller}/{action}",
            defaults: new { controller = "Auth", action = "Index" }
        );
            routes.MapRoute(
        name: "AuthControllerCall",
        url: "{controller}/{action}/{code}",
        defaults: new { controller = "Auth", action = "Callback", code = UrlParameter.Optional }
    );


            //   routes.MapRoute(
            //    name: "PriceEstimate1",
            //    url: "{controller}/{action}/{startlat}/{startlng}/{endlat}/{endlng}",
            //    defaults: new { controller = "PriceEstimate", action = "Estimate", startlat = UrlParameter.Optional, startlng = UrlParameter.Optional, endlat = UrlParameter.Optional, endlng = UrlParameter.Optional }
            //);
            //   routes.MapRoute(
            //    name: "PriceEstimate",
            //    url: "estimate/{id}",
            //    defaults: new { controller = "PriceEstimate", action = "Estimate" }
            //);
        }
    }
}