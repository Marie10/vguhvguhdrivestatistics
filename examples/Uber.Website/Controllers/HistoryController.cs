﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Uber.Website.Helpers;

namespace Uber.Website.Controllers
{
    public class HistoryController : Controller
    {
        // GET: History
        [HttpGet]
        public async Task<ActionResult> GetHistory()
        {
            int offset = 1; int limit = 10;
            var auth = CookieHelper.GetAccessToken();
            if (auth == null)
            {
                return RedirectToAction("Index", "Auth");
            }

            var uberClient = UberClientHelper.Get(auth.Value);
            var userHistory = await uberClient.GetUserActivityAsync(offset,limit);

            return View("TableView", userHistory.Data.History);
        }
   
    }
}