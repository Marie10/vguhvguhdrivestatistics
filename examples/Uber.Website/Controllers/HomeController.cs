﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Uber.Website.Helpers;

namespace Uber.Website.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            return View();
          
        }


        public async Task<ActionResult> Estimate( float startLatitude, float startLongitude, float endLatitude, float endLongitude)
        {// 50.469278, 30.513891
            //50.476797, 30.631161
            //return View();
            var uberClient = UberClientHelper.Get();
            var prices = await uberClient.GetPriceEstimateAsync(50.469278f, 30.513891f, 50.476797f, 30.631161f);

            return View("Results", prices.Data);
        }
    }
}
