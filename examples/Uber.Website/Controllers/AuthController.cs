﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Uber.Website.Helpers;

namespace Uber.Website.Controllers
{
    public class AuthController : Controller
    {
        public ActionResult Index()

        {
            //var scopes = new List<string> { "profile+", "history_lite+", "request" };
            var scopes = new List<string> { "history+","profile+" };

            string redirectUrl = null;
            var uberClient = UberClientHelper.GetAuth();
            var response = uberClient.GetAuthorizeUrl(scopes, null,redirectUrl);

            // return View("Index", (object)response);
            return Redirect(response);
        }

        public async Task<ActionResult> Callback(string code)

        {
            var uberClient = UberClientHelper.GetAuth();
            var accessToken = await uberClient.GetAccessTokenAsync(code, "http://localhost:65463//auth/callback");

            if (accessToken == null || string.IsNullOrWhiteSpace(accessToken.Value))
            {
                return RedirectToAction("Index");
            }
           CookieHelper.SetAccessTokenTest(accessToken, Response);
       
            return View();
        }
    }
}
