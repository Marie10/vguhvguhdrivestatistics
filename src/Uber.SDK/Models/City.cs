﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uber.SDK.Models
{
   public class City
    {
        [JsonProperty(PropertyName = "latitude")]
        public float Latitude { get; set; }
        [JsonProperty(PropertyName = "display_name")]
        public string DisplayName { get; set; }
        [JsonProperty(PropertyName = "longitude")]
        public float Longitude { get; set; }
    }
}
